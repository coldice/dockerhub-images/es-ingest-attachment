[Git Repository](https://gitlab.com/coldice/dockerhub-images/es-ingest-attachment)

# Elasticsearch with Ingest Attachment
This image is based on the [official](https://www.docker.elastic.co/) Elasticsearch image and only adds the Ingest Attachment plugin.

## Example (docker-compose)
```yaml
version: "3"
services:
    elasticsearch:
        image: cldc/es-ingest-attachment:latest
        restart: unless-stopped
        environment:
        - discovery.type=single-node
        - bootstrap.memory_lock=true
        ulimits:
        memlock:
            soft: -1
            hard: -1
        volumes:
        - elasticsearch:/usr/share/elasticsearch/data
```
